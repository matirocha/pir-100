#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Descripción:	Implementación de la técnica de Algoritmos de reemplazo GDS
#			Basada en el uso de colas con prioridad
#			GDS elimina el nodo con menor score H
# Autor: Gabriel Tolosa - Matías Rocha - UNLu
# Modificado: 5/4/17


import Queue
import time
import parametros

class Elemento(object):
    def __init__(self, score, query):
        self.score = float("{0:.4f}".format(score))
        self.query = query
        return
        
	def __cmp__(self, other):
		return cmp(self.score, other.score)

class GDSCache():
	
	def __init__(self, size):
		self.max_size = size
		self.size = 0
		self.cache = {}
		self.cacheCost = self.cargarCostos()
		self.cachePQ = Queue.PriorityQueue()
		self.L = 0
		self.hits = 0
		self.miss = 0
		self.corrects = 0
		self.totalHIT = 0
		
		
	def insert(self, query):		
		if (self.size >= self.max_size):
			#Si el tamaño del cache actual esta al tope, debo desalojar uno
				element = self.cachePQ.get()	#Obtengo el elemento de menor score del cachePQ
				clave = element.query	#Obtengo su query
				hValue = element.score	#Obtengo su score
				self.size -= 1 #Decremento en 1 el caché
				self.L = hValue #Asigno el L con el menor
				del self.cache[clave] #Borro de mi diccionario cache el elemento
				
		#Inserto el nuevo elemento
		score = float(self.L) + (float(self.cacheCost[query]) / 1)	#Calculo el score
		self.cache[query] = float("{0:.4f}".format(score))
		self.size += 1 #Incremento en 1 el caché
		e = Elemento(score, query)		#Elemento a ingresar
		self.cachePQ.put(e)
		return
					
	def update(self, query):

			aux = []
			element = self.cachePQ.get()	#Obtengo el elemento de menor score del cachePQ
			clave = element.query	#Obtengo su query
			hValue = element.score	#Obtengo su score
			while (clave != query):
				#Mientras no sea la query que quiero actualizar pido otro
				aux.append(element) #Guardo el que saco en un auxiliar
				element = self.cachePQ.get()	#Obtengo otro
				clave = element.query	#Obtengo su query
				hValue = element.score	#Obtengo su score

			for e in aux:
				#Para todos los elementos que saque de mi Cola, los vuelvo a encolar
				self.cachePQ.put(e)	#Encolo elemento	

			#Actualizo el Hvalue del elemento
			score = float(self.L) + (float(self.cacheCost[query]) / 1)	#Calculo el score
			e = Elemento(score, query)		#Elemento a ingresar
			self.cachePQ.put(e)	#Encolo elemento
			self.cache[query] = float("{0:.4f}".format(score)) #Actualizo el score en mi cache
			return
			
	def cargarCostos(self):
		p = parametros.Parameters()
		path = p.getPathCosts()
		costos = {}
		try:
			archivo = open (path, "r")
		except e:
			print "Error de lectura: " + e
		linea = archivo.readline()	#Leo linea del archivo de info
		print "Cargando costos en memoria"
		while linea != "":
			split = linea.split("\t:\t")	#Spliteo por tab
			costos[split[0].strip()] = float(split[1].strip())	#Cargo en la query con su respectivo costo
			linea = archivo.readline()	#Leo linea del archivo de info
		print "Proceso terminado"
		archivo.close()
		return costos
		
				
	def accessCache(self, query, hitBand):
		query = query.strip()
		tf = 0
		try:
			costo = self.cacheCost[query.strip()]	#Si el costo existe se lo asigno
		except:
			costo = 0	#Si no existe le pongo 0
		if float(costo) != 0:
		#Si el costo no es cero
			
			self.corrects += 1
			if (self.cache.has_key(query)):
				#Si la query esta en la caché, es decir hay un HIT actualizo el score	
				self.update(query)
				if hitBand:
					#Si la bandera de hits esta activa, cuento un hit
					self.hits += 1
			else:
			#Si el elemento no esta en el caché, lo agrego
				self.insert(query.strip())
				tf = costo 	#Si el query no esta en el cache, simulo costo con el de terrier
				if hitBand:
					#Si la bandera de hits esta activa, cuento un miss
					self.miss += 1
		return tf
			
				
	def getTotalHit(self):
		total = float(self.hits) / (float(self.hits) + float(self.miss))
		return total
		
	def getTotalCorrects(self):
		return self.corrects
