#! /usr/bin/env python
# encoding: UTF-8
from argparse import ArgumentParser
import ConfigParser
import os 

class Parametros:
	#Variable global utilizada para parsear los parametros
	global arg
	
	""" -*- """
	def __init__(self):
		"""Constructor de la clase."""
		
		# Descripción de lo que hace el ejercicio
		parser = ArgumentParser(description='%(prog)s es un script que permite implementar la tecnica GDS de reemplazos en cache')

		parser.add_argument("longQuery", type=int, help="Cantidad de terminos de la query")
		
		parser.add_argument("sizeBloque", type=int, help="Cantidad de queries de calentamiento (warm-up)")
				
		# Por último parsear los argumentos
		self.arg = parser.parse_args()
		
	def getLongQuery(self):
		return self.arg.longQuery
		
	def getBloque(self):
		return self.arg.sizeBloque
			
class Properties:

	def __init__(self):
		config = ConfigParser.ConfigParser()
		path = os.path.dirname(os.path.realpath(__file__))
		config.read(path + "/properties.ini")
		self.pathQuery = config.get('PATH', 'QUERY')
		self.pathVocabulary = config.get('PATH', 'VOCABULARY')

	def getPathQuery(self):
		return self.pathQuery
		
	def getPathVocabulary(self):
		return self.pathVocabulary
	
	
		
