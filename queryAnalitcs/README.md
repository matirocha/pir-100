## Sinopsis

Script que dado un vocabulario y un querylog procesa diferente tipos de estadisiticas para querys Singletons o No SIngletons de la cantidad de terminos que se le pasen por parametro.


## Motivacion

Script realizado bajo el proyecto de pasantía rentada de la UNLU


## Ejecucion

Para la ejecución del mismo:

Ejecutar main.py -longitudQuery -TamañoBloque

Editar el archivo /lib/conf/properties.ini con el path del archivo de querys y de vocabulario.

## Autor

Matías Rocha
