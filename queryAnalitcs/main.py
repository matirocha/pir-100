#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os  
import re
import matplotlib.pyplot as plt
#from graphviz import Digraph

path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(path + "/lib")

import parametros

def createGraph(querys):
	#Creo mi grafo vacio
	print "Analizando datos para crear Grafo"
	grafo = {}
	#Para cada vertice
	for q in querys.keys():
		split = q.strip().split(" ")
		origen, destino = split[0], split[1]
		try:
			grafo[origen].append(destino)
		except:
			grafo[origen] = []
			grafo[origen].append(destino)
	return grafo

def printGraph(grafo, comentario='Grafo'):
	style = {'nodeNS': {
	    'fontname': 'Helvetica',
	    'fontcolor': 'white',
	    'color': 'white',
	    'style': 'filled',
	    'fillcolor': '#006699',
	},
	'nodeS': {
	    'fontname': 'Helvetica',
	    'fontcolor': 'black',
	    'color': 'white',
	}}
	print "Creando Grafo"

	dot = Digraph(comment=comentario)
	for vertice, listaAdyacencia in grafo.items():
		dot.attr('node', style["nodeNS"])
		dot.node(str(vertice))
		for verticeAdyacente in listaAdyacencia:
			dot.edge(str(vertice), str(verticeAdyacente))
	dot.graph_attr['rankdir'] = 'LR'
	return dot

def main():

	#Instancio la clase parametros
	p = parametros.Parametros()
	lenQ = p.getLongQuery()
	blok = p.getBloque()
	
	p = parametros.Properties()
	pathV = p.getPathVocabulary()
	pathQ = p.getPathQuery()

	##Carga de memoria el vocabulario con su DF
	vocabulario = {}
	vocab = open(pathV, "r")
	print "Leyendo el Vocabulario"
	linea = vocab.readline()
	while linea != '':
		split = linea.split(',')
		df = split[1].split(' ')[1].replace("Nt=", "")
		vocabulario[split[0].strip()] = int(df.strip())
		linea = vocab.readline()

	##FIn de carga del vocabulario###

	
	frec_x_cant = {}	#Diccionario que contiene la frecuencia de las querys
	terms_x_query = {}	#Diccionario que contiene la cantidad querys para una determinada cantidad de terminos por query

	frec_qL = {}	#Diccionario que cuenta la frec de una query para la long pedida
	frec_qT = {}	#Diccionario que cuenta la frec de una query total
	cBloque = 0 #Contador de bloque
	cTotal = 0

	#Sets
	singletonsT = set()	#Singletons acumulados
	NoSingletonsT = set()
	singletonsL = set()	#Singletons local
	singletonsAL = set()	#Singleton auxiliar local para comparar
	singletonsAT = set()	#Singleton auxiliar total para comparar
	listaCambios = []	#Lista que contendra las querys 
	psL = 0	#% de Singletons Local
	psT = 0	#% de Singletons Total
	tL = 0	#Tasa de conversion local
	tT = 0	#Tasa de conversion total
	cQ_q = 0	#Contador de querys
	c = 0	#Contador de querys

	statics = [] #Lista con % de Singletons por bloque (Local - Total)
	absoluto = [] #Lista con valores absolutos de Singletons por bloque
	tasa = []	#Lista con tasa de conversion por bloque (Local - Total)
	conver = []	#Lista con valores ablosutos de conversion de Singleton a No-Singleton
	terminos = {}	#Diccionario que contendra una lista con [contador, listaDeQuerys]

	#Lectura del archivo y normalizacion de querys
	print "Leyendo archivo de querys"
	archivo = open (pathQ, "r")
	query = archivo.readline()	
	while query != '':
		query = query.strip()
		split = query.split(" ")
		terms = len(split)
		band = True
		for t in split:
			if not(vocabulario.has_key(t)):
				band = False

		if band:

			#Frec de las querys
			try:
				frec_x_cant[query] += 1
			except:
				frec_x_cant[query] = 1

			#Frec por cantidad de terminos en query
			try:
				terms_x_query[terms] += 1
			except:
				terms_x_query[terms] = 1

			if (lenQ == terms):
				c += 1
				cBloque += 1
				#Si la cantidad de terminos es igual a la indicada

				#Sumo a mi contador local de frecuencias
				try:
					frec_qL[query] += 1
				except:
					frec_qL[query] = 1

				try:
					frec_qT[query] += 1
				except:
					frec_qT[query] = 1

				if (cBloque == blok):
					#Cuando llego al tope del bloque
					cTotal += cBloque
					for q in frec_qL:
						#Recorro mi diccionario local de frecuencias de querys con lenQ
						if frec_qL[q] == 1:
							#Si es 1, es Singleton
							singletonsL.add(q)	#Agrego al set de singletons de manera local por bloque
					
					for q in frec_qT:
						if frec_qT[q] == 1:
							#Si es 1, es Singleton
							singletonsT.add(q)	#Agrego al set de singletons de manera local por bloque

	
					#Saco porcentajes
					psL = float(len(singletonsL)) / float(cBloque)	#% de singletons de manera local
					psT = float(len(singletonsT)) / float(cTotal)		#% de singletons de manera total
					aL = len(singletonsL)
					aT = len(singletonsT)
					statics.append((psL,psT))	#Agrego a mi lista de estadisiticas por bloque
					absoluto.append((aL,aT))	#Agrego a mi lista de absoluto por bloque
					#Tasa de conversion
					#tL = len(singletonsAL - singletonsL)	#Calculo cuantas querys dejaron de ser Singleton por bloque
					cT = len(singletonsAT - singletonsT)	#Calculo cuantas querys dejaron de ser Singleton acumulada
					tT = float(cT) / float(cTotal)	#Calculo la tasa de cambio sobre el total de querys procesados con la long dada
					listaCambios.append(((singletonsAT - singletonsT),(singletonsAL - singletonsL))) #Agrego el set que me da la resta a una lista que luego graficare

					tasa.append((cT,tT))	#Agrego a mi lista de Tasa de Conversion
					
					cBloque = 0	#seteo en 0 mi contador de bloque
					singletonsAT = set(singletonsT)	#Cambio de set al auxiliar
					singletonsAL = set(singletonsL)
					singletonsL = set()	#Pongo en cero el Local
					singletonsT = set()
					frec_qL = {}	#Seteo en cero

				
		query = archivo.readline()
	archivo.close()
	#Fin lectura del archivo de queries

	#Computo los datos que me quedaron colgados
	if cBloque > 0:
		#Cuando llego al tope del bloque
		cTotal += cBloque
		for q in frec_qL:
			#Recorro mi diccionario local de frecuencias de querys con lenQ
			if frec_qL[q] == 1:
				#Si es 1, es Singleton
				singletonsL.add(q)	#Agrego al set de singletons de manera local por bloque
							
		for q in frec_qT:
			if frec_qT[q] == 1:
				#Si es 1, es Singleton
				singletonsT.add(q)	#Agrego al set de singletons de manera local por bloque
			else:
				#Si la frecuencia no es 1, es NoSingleton
				NoSingletonsT.add(q)
			split = q.split(" ")
			for i in split:
				try:
					terminos[i][0] += (1*frec_qT[q])
					terminos[i][1].append(q)
				except:
					terminos[i] = [(1*frec_qT[q]), [q]]	#terminos[termino] = [contador de frec, [lista de querys donde esta]]

		#Saco porcentajes
		psL = float(len(singletonsL)) / float(cBloque)	#% de singletons de manera local
		psT = float(len(singletonsT)) / float(cTotal)		#% de singletons de manera total
		aL = len(singletonsL)
		aT = len(singletonsT)
		statics.append((psL,psT))	#Agrego a mi lista de estadisiticas por bloque
		absoluto.append((aL,aT))	#Agrego a mi lista de absoluto por bloque
		#Tasa de conversion
		cT = len(singletonsAT - singletonsT)	#Calculo cuantas querys dejaron de ser Singleton acumulada
		tT = float(cT) / float(cTotal)
		#Distribucion de frecuencias sobre los cambios
		listaCambios.append(((singletonsAT - singletonsT),(singletonsAL - singletonsL)))	#Agrego el set que me da la resta a una lista que luego graficare

		tasa.append((cT,tT))	#Agrego a mi lista de Tasa de Conversion

	#Creo directorio de salida si no existe

	pathSalida = os.path.dirname(os.path.realpath(__file__))
	try:
		os.makedirs(pathSalida + "/salida/")
  	except OSError:
  		pass

  	try:
		os.makedirs(pathSalida + "/salida/graficos")
  	except OSError:
  		pass

  	singletons_1t = {}
  	NoSingletons_1t = {}
  	#Me quedo con terminos Singleton y No-Singeltons con longQ = 1
  	for q in frec_x_cant:
  		q = q.strip()
  		cant = len(q.split(" "))
  		if (cant == 1):
  			if frec_x_cant[q] == 1:
  				singletons_1t[q] = 0
  			else:
  				NoSingletons_1t[q] = 0

  	print "Ploteando"
  	
  	cG = 0
  	#Grafico Disrtibucion de frecuencias
  	for tupla in listaCambios:
  		#Para cada tupla (cambioSingetonsXBloque,cambioSingletonsXAcum)
  		fig, axs = plt.subplots(2,1, figsize=(16, 12))
		fig.suptitle("Distribucion de Frecuencias para terminos Sinletons que se convirtieron en No-Singletons")
		axs = axs.ravel()
  		i = 0
  		for setS in tupla:
  			#Por cada set de cambios
  			datos = {}
  			f = 0
  			for q in setS:
  				split = q.strip().split(" ")
  				for s in split:
  					f += int(terminos[s][0]) #Frec frec(t1 + t2 + tn)
  				datos[q] = f

			datos_s = sorted(datos.values(), reverse=True)
			axs[i].plot(datos_s)
			if i == 0:
				axs[i].set_title("Acumulado")
			else:
				axs[i].set_title("Local por bloque")
			axs[i].set_xlabel("Terminos")   # Establece el título del eje x
			axs[i].set_ylabel("Frecuencia (T1 + Tn)")   # Establece el título del eje y
			i += 1

		plt.savefig('salida/graficos/graficoBloque%d.png' %(cG))
		plt.close()
		cG += 1


	#Grafico Distribucion de DF de Singletons y NoSingletons
	dfS = {} #Diccionario con el DF acumulado por query DF = DFt1 + DFt2
	fS = {}	#Diccionario con la frecuencia acumulada por query
	df_s = [] #Lista con todos las sumatorias de DF de SIngletons
	f_s = []	#Lista con todos las sumatorias de Frec de SIngletons
	for q in singletonsT:
		#Para todas las querys Singleton
		ac = 0
		ac2 = 0
		split = q.strip().split(" ")
		for t in split:
			#Acumulo el DF de todos los terminos de la query
			ac += vocabulario[t]
			#Acumulo frecuencia
			ac2 += terminos[t][0]
		#Cuento cuantos hay querys hay por DF acumulado
		df_s.append(ac)
		f_s.append(ac2)
		try:
			dfS[ac] += 1
		except:
			dfS[ac] = 1

		try:
			fS[ac2] += 1
		except:
			fS[ac2] = 1

	dfNS = {}
	fNS = {}
	df_ns = [] #Lista con todos las sumatorias de DF de No-Singletons
	f_ns = []	#Lista con todos las sumatorias de Frec de No-SIngletons
	for q in NoSingletonsT:
		#Para todas las querys Singleton
		ac = 0
		ac2 = 0
		split = q.strip().split(" ")
		for t in split:
			#Acumulo el DF de todos los terminos de la query
			ac += vocabulario[t]
			#Acumulo frecuencia
			ac2 += terminos[t][0]
		#Cuento cuantos hay querys hay por DF acumulado
		df_ns.append(ac)
		f_ns.append(ac2)
		try:
			dfNS[ac] += 1
		except:
			dfNS[ac] = 1

		try:
			fNS[ac2] += 1
		except:
			fNS[ac2] = 1



	#Grafico DF acumulado para todos los querys
	fig = plt.figure(figsize=(18, 14))
	fig.suptitle("Sumatoria de DF para querys de %d terminos" %(lenQ))
	ax1 = fig.add_subplot(211)
	ax1.plot(df_s, 'ro', label = "Singletons")
	ax1.plot(df_ns, 'go', label = "No-Singletons")
	ax1.legend(loc="upper right", prop={'size':9}) 
	ax1.set_xlabel("Querys")   # Establece el título del eje x
	ax1.set_ylabel("Sumatoria DF")   # Establece el título del eje y
	ax1.set_title("Singletons y No Singletons")
	#Escala logaritmica
	ax2 = fig.add_subplot(212)
	ax2.plot(df_s, 'ro', label = "Singletons")
	ax2.plot(df_ns, 'go', label = "No-Singletons")
	ax2.legend(loc="upper right", prop={'size':9}) 
	ax2.set_title("Singletons y No-Singletons (LOG)")
	ax2.set_xlabel("Querys")   # Establece el título del eje x
	ax2.set_ylabel("Sumatoria DF (LOG)")   # Establece el título del eje y
	ax2.set_yscale('log')
	plt.savefig('salida/graficos/DF_2.png')
	plt.close()

	#Grafico DF acumulado para todos los querys
	fig = plt.figure(figsize=(18, 14))
	fig.suptitle("Sumatoria de la frec para querys de %d terminos" %(lenQ))
	ax1 = fig.add_subplot(211)
	ax1.plot(f_s, 'ro', label = "Singletons")
	ax1.plot(f_ns, 'go', label = "No-Singletons")
	ax1.legend(loc="upper right", prop={'size':9}) 
	ax1.set_xlabel("Querys")   # Establece el título del eje x
	ax1.set_ylabel("Sumatoria Frec")   # Establece el título del eje y
	ax1.set_title("Singletons y No Singletons")
	#Escala logaritmica
	ax2 = fig.add_subplot(212)
	ax2.plot(f_s, 'ro', label = "Singletons")
	ax2.plot(f_ns, 'go', label = "No-Singletons")
	ax2.legend(loc="upper right", prop={'size':9}) 
	ax2.set_title("Singletons y No-Singletons (LOG)")
	ax2.set_xlabel("Querys")   # Establece el título del eje x
	ax2.set_ylabel("Sumatoria Frec (LOG)")   # Establece el título del eje y
	ax2.set_yscale('log')
	plt.savefig('salida/graficos/F_2.png')
	plt.close()

  	#Me quedo con terminos mas frecuentes en las querys
  	ordenados = terminos.items()
	ordenados.sort(key=lambda x:x[1][0], reverse=True)

	#Me quedo con los terminos con mayor cantidad de vertices con parametro = 100
	plot = {}
	for i in range(0,10):
		l1 = ordenados[i][1][1]
		for q in l1:
			plot[q] = 0

  	#Grafo
  	#grafo = createGraph(plot)
  	#printGraph(grafo, 'Grafo').render('ejemplo_graphviz')

	#Estadisticas

	print "Creando logs de estadisticas"
	stds = open("salida/estadisticas.txt", "w")
	stds.write('Porcentaje de Singletons en el QUERY LOG\n\n')
	stds.write('Longitud de query: %d\n' %(lenQ))
	stds.write('Total de querys: %d\n' %(c))
	stds.write('Total de querys unicas: %d\n' %(len(frec_qT)))
	stds.write('Total de Singletons: %d\n' %(len(singletonsT)))
	stds.write('Total de No-Singletons: %d\n' %(len(NoSingletonsT)))
	stds.write('Total de terminos unicos: %d\n' %(len(terminos)))
	stds.write('Tamaño de Bloque de querys: %d\n\n' %(blok))
	stds.write("Nro de Bloque\tC-Sing(L)\t% Singleton(L)\tC-Sing(A)\t% Singleton(A)\tConversion(Abs)\t\tConversion(Tasa)\n")
	for i in range(0,len(statics)):
		stds.write("%d\t\t%d\t\t%0.3f\t\t%d\t\t%0.3f\t\t%d\t\t\t%0.3f\n" %(i, absoluto[i][0], statics[i][0],absoluto[i][1], statics[i][1], tasa[i][0], tasa[i][1]))



if __name__ == "__main__":
   sys.exit(main())