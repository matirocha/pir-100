#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os  
import re
import matplotlib.pyplot as plt

def main(args):
	path = args[1] #Path de las queries 1
	pathV = args[2] #Path del Vocabulario 2
	term_x_frec = {}
	frec_x_cant = {}
	terminos = {}
	cant = 0
	archivo = open (path, "r")
	pathSalida = os.path.dirname(os.path.realpath(__file__))

	try:
		os.makedirs(pathSalida + "/salida/")
  	except OSError:
  		pass


	norm = open("salida/queryLogNormalizado.txt", "w")
	print "Leyendo archivo de querys"
	##Lectura del archivo y normalizacion de querys
	linea = archivo.readline()
	while linea != '':
		q = ""
		cant += 1
		split = linea.strip().split(" ")
		for s in sorted(split):
			q += s + " "
		q = q.strip()
		norm.write("%s\n" %(q.strip()))
		#Frec terminos Singleton
		try:
			frec_x_cant[q] += 1
		except:
			frec_x_cant[q] = 1

		linea = archivo.readline()
	archivo.close()
	norm.close()
	###Fin de la normalizacion###


	##Carga de memoria el vocabulario con su DF
	vocabulario = {}
	vocab = open(pathV, "r")
	print "Leyendo el Vocabulario"
	linea = vocab.readline()
	while linea != '':
		split = linea.split(',')
		df = split[1].split(' ')[1].replace("Nt=", "")
		vocabulario[split[0].strip()] = int(df.strip())
		linea = vocab.readline()

	##FIn de carga del vocabulario###

	print "Procesando estadisticas"
	frec_s = {} #Diccionario de frecuencia de la cantidad de terminos de los Singleton
	frec_ns = {} #Diccionario de frecuencia de la cantidad de terminos de los No-Singleton unicos
	total_ns = {} #Diccionario de frecuencia totales de la cantidad de terminos de los No-Singleton
	singletons = {}
	NoSingletons = {}
	df_s = {} #Diccionario con la cantidad de terminos por DF de los Signleton
	df_ns = {} #Diccionario con la cantidad de terminos por DF de los No-Signleton
	df_s_2 = {} #Diccionario querys singletons de 2 terminos y su correspondeinte DF
	df_ns_2 = {} #Diccionario querys No-singletons de 2 terminos y su correspondeinte DF
	s2 = 0
	ns2 = 0
	cc = 0


	for q in frec_x_cant:
		#frecuencia terminos sueltos
		terms = q.strip().split(" ")
		cant_terms = len(terms)
		for ti in terms:
			try:
				terminos[ti] += 1
			except:
				terminos[ti] = 1

		if cant_terms >= 10:
			cant_terms = 10

		if frec_x_cant[q] == 1:
			#Si la frecuencia de la query es 1, es SINGLETON

			try:
				frec_s[cant_terms] += 1
			except:
				frec_s[cant_terms] = 1

			#Armo el DF = frec para querys singleton de 1 termino solo
			if (cant_terms == 1) and (q.strip() in vocabulario):
				try:
					df_s[vocabulario[q.strip()]] += 1
				except:
					df_s[vocabulario[q.strip()]] = 1
				singletons[q.strip()] = 1



			#Armo el DF = frec para querys singleton de 2 terminos
			
			if (cant_terms == 2):
					df_list = []
					band = True
					for t in terms:
						if t in vocabulario:
							df_list.append(vocabulario[t.strip()])
						else:
							band = False
							df_list.append(0)

						if band:
							s2 += 1
					df_s_2[q.strip()] = df_list




		else:
			#Si la frecuencia de la query es > 1, es NO-SINGLETON
			try:
				frec_ns[cant_terms] += 1
			except:
				frec_ns[cant_terms] = 1

			try:
				total_ns[cant_terms] += frec_x_cant[q]
			except:
				total_ns[cant_terms] = frec_x_cant[q]


			#Armo el DF = frec para querys No-singleton de 1 termino solo
			if (cant_terms == 1) and (q.strip() in vocabulario):
				try:
					df_ns[vocabulario[q.strip()]] += 1
				except:
					df_ns[vocabulario[q.strip()]] = 1

				NoSingletons[q.strip()] = 1

			#Armo el DF = frec para querys No-singleton de 2 terminos
			if (cant_terms == 2):
					cc += 1
					df_list = []
					band = True
					for t in terms:
						if t in vocabulario:
							df_list.append(vocabulario[t.strip()])
						else:
							band = False
							df_list.append(0)

					if band:
						df_ns_2[q.strip()] = df_list

	
	print "Ultimo paso"
	t3 = 0
	t3_1 = []
	t3_2 = []
	t2 = 0
	t2_1 = []
	t2_2 = []
	t1 = 0
	t1_1 = []
	t1_2 = []
	t0 = 0
	t0_1 = []
	t0_2 = []

	#Singletons con terminos No-Singletons
	s2_1 = []	#Uno de los dos es NS
	s2_2 = []	#Ambos son NS

	#No Sinlgetons con terminos singletons
	ns2_1 = []	#Uno de los dos es S
	ns2_2 = []	#Ambos son S

	#Veo si alguno de los Singletons tiene terminos No-Singletons
	for query in df_s_2:
		query = query.strip()
		terms = query.split(" ")
		band1 = False
		band2 = False
		
		if NoSingletons.has_key(terms[0]) or NoSingletons.has_key(terms[1]):
			band1 = True

		if NoSingletons.has_key(terms[0]) and NoSingletons.has_key(terms[1]):
			band2 = True

		if band1:
			s2_1.append(df_s_2[query])

		if band2:
			s2_2.append(df_s_2[query])



	#Veo si alguno de los terminos No-Singletons son Singletons
	for query in df_ns_2:
		query = query.strip()
		terms = query.split(" ")
		band1 = False
		band2 = False
		if singletons.has_key(terms[0]):
			band1 = True


		if singletons.has_key(terms[1]):
			band2 = True


		if band1 or band2:
			ns2_1.append(df_ns_2[query])



		if band1 and band2:
			xs = 3
			t3 += 1
			ns2_2.append(df_ns_2[query])
			t3_1.append(df_ns_2[query][0])
			t3_2.append(df_ns_2[query][1])
		elif band1:
			xs = 1
			t1 += 1
			t1_1.append(df_ns_2[query][0])
			t1_2.append(df_ns_2[query][1])
		elif band2:
			xs = 2
			t2 += 1
			t2_1.append(df_ns_2[query][0])
			t2_2.append(df_ns_2[query][1])
		else:
			xs = 0
			t0 += 1
			t0_1.append(df_ns_2[query][0])
			t0_2.append(df_ns_2[query][1])
			
		df_ns_2[query].append(xs)

	print "Graficando"


	#Grafica No Singletons con terminos Singletons
	m1 = []
	m2 = []
	for coord in ns2_1:
		ma = max(coord[0], coord[1])
		mi = min(coord[0], coord[1])
		m1.append(ma)
		m2.append(mi)
	
	fig = plt.figure(figsize=(18,18))
	fig.suptitle("DF para No-Singletons de 2 terminos con terminos Singleton")
	m = max(max(m1),max(m2))
	ax1 = fig.add_subplot(211)
	ax1.plot(m1, 'ro', label = "DF mayor")
	ax1.plot(m2, 'go', label = "DF menor")
	ax1.legend(loc="upper right", prop={'size':8}) 
	ax1.set_xlabel("Querys")   # Establece el título del eje x
	ax1.set_ylabel("DF")   # Establece el título del eje y
	ax1.set_ylim([0,m])

	m1 = []
	m2 = []
	for coord in ns2_2:
		ma = max(coord[0], coord[1])
		mi = min(coord[0], coord[1])
		m1.append(ma)
		m2.append(mi)
	
	m = max(max(m1),max(m2))
	ax2 = fig.add_subplot(212)
	ax2.plot(m1, 'ro', label = "DF mayor")
	ax2.plot(m2, 'go', label = "DF menor")
	ax2.legend(loc="upper right", prop={'size':8}) 
	ax2.set_ylim([0,m])
	ax2.set_xlabel("Querys")   # Establece el título del eje x
	ax2.set_ylabel("DF")   # Establece el título del eje y
	ax2.set_ylim([0,m])
	ax1.title.set_text('Uno de los terminos es Singleton')
	ax2.title.set_text('Ambos terminos son Singleton')
	plt.savefig('salida/NS2.png')
	plt.close()


	#Grafica Singletons con terminos No-Singletons
	m1 = []
	m2 = []
	for coord in s2_1:
		ma = max(coord[0], coord[1])
		mi = min(coord[0], coord[1])
		m1.append(ma)
		m2.append(mi)
	
	fig = plt.figure(figsize=(18,18))
	fig.suptitle("DF para Singletons de 2 terminos con terminos No-Singleton")
	m = max(max(m1),max(m2))
	ax1 = fig.add_subplot(211)
	ax1.plot(m1, 'ro', label = "DF mayor")
	ax1.plot(m2, 'go', label = "DF menor")
	ax1.legend(loc="upper right", prop={'size':8}) 
	ax1.set_xlabel("Querys")   # Establece el título del eje x
	ax1.set_ylabel("DF")   # Establece el título del eje y
	ax1.set_ylim([0,m])
	m1 = []
	m2 = []
	for coord in s2_2:
		ma = max(coord[0], coord[1])
		mi = min(coord[0], coord[1])
		m1.append(ma)
		m2.append(mi)
	
	m = max(max(m1),max(m2))
	ax2 = fig.add_subplot(212)
	ax2.plot(m1, 'ro', label = "DF mayor")
	ax2.plot(m2, 'go', label = "DF menor")
	ax2.legend(loc="upper right", prop={'size':8}) 
	ax2.set_ylim([0,m])
	ax2.set_xlabel("Querys")   # Establece el título del eje x
	ax2.set_ylabel("DF")   # Establece el título del eje y
	ax2.set_ylim([0,m])
	ax1.title.set_text('Uno de los terminos es No.Singleton')
	ax2.title.set_text('Ambos terminos son No-Singleton')
	plt.savefig('salida/S2.png')
	plt.close()





	#Grafica de puntos

	m1 = []
	m2 = []
	for coord in df_ns_2.values():
		ma = max(coord[0], coord[1])
		mi = min(coord[0], coord[1])
		m1.append(ma)
		m2.append(mi)
		
	m = max(max(m1),max(m2))
	fig = plt.figure(figsize=(18,18))
	fig.suptitle("DF para No-Singletons de 2 terminos")
	
	ax1 = fig.add_subplot(211)
	ax1.plot(m1, 'ro', label = "DF mayor")
	ax1.plot(m2, 'go', label = "DF menor")
	ax1.legend(loc="upper right", prop={'size':8}) 
	ax1.set_yscale('log')
	ax1.set_xlabel("Querys")   # Establece el título del eje x
	ax1.set_ylabel("DF (log)")   # Establece el título del eje y
	ax2 = fig.add_subplot(212)
	ax2.plot(m1, 'ro', label = "DF mayor")
	ax2.plot(m2, 'go', label = "DF menor")
	ax2.legend(loc="upper right", prop={'size':8}) 
	ax2.set_ylim([0,m])
	ax2.set_xlabel("Querys")   # Establece el título del eje x
	ax2.set_ylabel("DF")   # Establece el título del eje y
	ax1.title.set_text('Escala logaritmica')
	ax2.title.set_text('Escala comun')
	plt.savefig('salida/DFpuntos.png')
	plt.close()

	#Ordeno para graficar DF
	m = max(max(t3_1), max(t3_2), max(t2_1), max(t2_2), max(t1_1), max(t1_2))
	t3_2.sort(reverse=True)
	t3_1.sort(reverse=True)
	t2_2.sort(reverse=True)
	t2_1.sort(reverse=True)
	t1_2.sort(reverse=True)
	t1_1.sort(reverse=True)
	
	plt.plot(t3_2)
	plt.plot(t3_1)
	plt.plot(t3_2, label = "DF Termino 2")
	plt.plot(t3_1, label = "DF Termino 1")
	plt.legend(loc="upper right", prop={'size':8}) 
	plt.title("DF para No-Singletons con 2 Terminos Singletons")   # Establece el título del gráfico
	plt.xlabel("Querys")   # Establece el título del eje x
	plt.ylabel("DF")   # Establece el título del eje y
	plt.ylim([0,m])
	plt.savefig('salida/2t.png')
	plt.close()

	
	plt.plot(t2_2)
	plt.plot(t2_1)
	plt.plot(t2_2, label = "DF Termino 2")
	plt.plot(t2_1, label = "DF Termino 1")
	plt.legend(loc="upper right", prop={'size':8})
	plt.title("DF para No-Singletons con 2do termino Singleton")   # Establece el título del gráfico
	plt.xlabel("Querys")   # Establece el título del eje x
	plt.ylabel("DF")   # Establece el título del eje y
	plt.ylim([0,m])
	plt.savefig('salida/2d.png')
	plt.close()
	

	plt.plot(t1_2)
	plt.plot(t1_1)
	plt.plot(t1_2, label = "DF Termino 2")
	plt.plot(t1_1, label = "DF Termino 1")
	plt.legend(loc="upper right", prop={'size':8})
	plt.title("DF para No-Singletons con 1er termino Singleton")   # Establece el título del gráfico
	plt.xlabel("Querys")   # Establece el título del eje x
	plt.ylabel("DF")   # Establece el título del eje y
	plt.ylim([0,m])
	plt.savefig('salida/1er.png')
	plt.close()


	#Los 3 juntos
	fig = plt.figure(figsize=(18,18))
	fig.suptitle("DF para No-Singletons de 2 terminos")
	ax1 = fig.add_subplot(311)
	ax1.plot(t3_2, label = "DF Termino 2")
	ax1.plot(t3_1, label = "DF Termino 1")
	ax1.legend(loc="upper right", prop={'size':12})
	#plt.title("DF para No-Singletons con 2do termino Singleton")   # Establece el título del gráfico
	ax1.set_xlabel("Querys")   # Establece el título del eje x
	ax1.set_ylabel("DF")   # Establece el título del eje y
	ax1.set_ylim([0,m])
	
	ax2 = fig.add_subplot(312)
	ax2.plot(t2_2, label = "DF Termino 2")
	ax2.plot(t2_1, label = "DF Termino 1")
	ax2.legend(loc="upper right", prop={'size':12})
	ax2.set_xlabel("Querys")   # Establece el título del eje x
	ax2.set_ylabel("DF")   # Establece el título del eje y
	ax2.set_ylim([0,m])
	
	ax3 = fig.add_subplot(313)
	ax3.plot(t1_2, label = "DF Termino 2")
	ax3.plot(t1_1, label = "DF Termino 1")
	ax3.legend(loc="upper right", prop={'size':12})
	ax3.set_xlabel("Querys")   # Establece el título del eje x
	ax3.set_ylabel("DF")   # Establece el título del eje y
	ax3.set_ylim([0,m])
	
	ax1.title.set_text('DF para No-Singletons con los 2 terminos Singleton')
	ax2.title.set_text('DF para No-Singletons con 2do termino Singleton')
	ax3.title.set_text('DF para No-Singletons con 1er termino Singleton')

	plt.savefig('salida/DF_terms_2.png')
	plt.close()
	
	print "Creando logs de estadisticas"
	cant = float(cant)
	s = 0
	ns = 0
	tns = 0
	stds = open("salida/estadisticasSingleton.txt", "w")
	singletons = sorted(frec_s, key=int)
	stds.write("Estadisticas Singletons y No-Singletons\n\n")
	stds.write('%s \t\t\t\t %s \t\t\t\t %s \t\t\t\t %s \t\t\t\t %s \t\t\t\t %s \t\t\t\t \n' %("Cant. terminos", "Singletons", "No-Singletons(T)", "No-Singletons(U)" , "% Sinletons", "% No-Singleton" ))
	for r in singletons:
		s += frec_s[r]
		ns += frec_ns[r]
		tns += total_ns[r]
		stds.write('%d \t\t\t\t %d \t\t\t\t %d \t\t\t\t %d \t\t\t\t %0.3f \t\t\t\t %0.3f \n' %(r, frec_s[r], frec_ns[r], total_ns[r], (float(frec_s[r])/float(frec_s[r]+total_ns[r])), (float(frec_ns[r])/float(frec_s[r]+total_ns[r]) )))
		
	stds.write("Total querys procesadas: %d \n" %(cant))
	stds.write("Total Singleton: %d \n" %(s))
	stds.write("Total No-Singleton unicas: %d \n" %(ns))
	stds.write("Total No-Singleton totales: %d \n\n" %(tns))
	stds.close()



	stds = open("salida/estadisticasSingleton2Terminos.txt", "w")
	stds.write("Singletons:\n\n")
	stds.write("Total: %d\n" %(len(df_s_2)))
	stds.write("Total de Sigletons en el vocabulario: %d\n" %(s2))
	stds.write("Query\t\t\t\t\t\tDF T1\t\tDf T2\n")
	for datos in df_s_2:
		stds.write("%s\t\t\t\t%s\t\t%s\n" %(str(datos), df_s_2[datos][0], df_s_2[datos][1]))

	stds = open("salida/estadisticasNoSingleton2Terminos.txt", "w")
	stds.write("No-Singletons:\n\n")
	stds.write("Total unicos: %d\n" %(cc))
	stds.write("Total unicos con terminos en el vocabulario: %d\n" %(len(df_ns_2)))
	stds.write("Querys con 2 terminos Sigletons: %d\n" %(t3))
	stds.write("Querys con el 1er terminos Sigletons: %d\n" %(t1))
	stds.write("Querys con el 2do terminos Sigletons: %d\n" %(t2))
	stds.write("Querys con ningun termino Sigletons: %d\n" %(t0))
	stds.write("Query\t\t\t\t\t\tDF T1\t\tDf T2\n")
	
	for datos in df_ns_2:
		stds.write("%s\t\t\t\t%s\t\t%s\t\t%d\n" %(str(datos), df_ns_2[datos][0], df_ns_2[datos][1], df_ns_2[datos][2]))

	stds.close()


	stds = open("salida/estadisticasQuerys1Terminos.txt", "w")	
	stds.write("DF de los Singleton: \n")
	df = sorted(df_s, key=int)
	for r in df:
		stds.write("%d \t %d\n" %(r, df_s[r]))


	stds.write("DF de los No-Singleton: \n")
	df = sorted(df_ns, key=int)
	for r in df:
		stds.write("%d \t %d\n" %(r, df_ns[r]))
	stds.close()

	st = open("salida/terminos.txt", "w")
	st.write("%s \t\t\t\t\t\t %s\n" %("Termino", "Frec"))
	#terms = sorted(terminos.items(), key=int)
	for key, value in sorted(terminos.iteritems(), key=lambda (v,k): (v,k)):
		st.write("%s: %d\n" % (key, value))
	st.close()
		
if __name__ == "__main__":
   sys.exit(main(sys.argv))
