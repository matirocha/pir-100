#! /usr/bin/env python
# encoding: UTF-8

import sys
import os  

def main(args):
	drt = args[1]
	i = 1
	size = 0
	c = 0
	path = "c"
	if not os.path.exists(path):
		os.mkdir(path)
	coleccion = open("c/coleccion-%d.txt" %(c), 'w+')
	for base, dirs, files in os.walk(drt):
		for f in files:
			a = os.path.join(base, f)
			size += os.path.getsize(a)
			archivo = open(a, 'r')
			contenido = archivo.read()
			archivo.close()
			linea = """<DOC>
<DOCNO>%d</DOCNO>
%s
</DOC>
""" %(i, contenido.strip())
			i += 1
			coleccion.write(linea)
			if ((size % 4000000000) <= 500):
				coleccion.close()
				c += 1
				coleccion = open("c/coleccion-%d.txt" %(c), 'w+')
	coleccion.close()
	path = "c/coleccion-%d.txt" %(c)
	if os.path.getsize(path) == 0:
		os.remove(path)
	print "Cant de archivos: %d" %(i)
	print "Tamaño total: %d bytes" %(size)

if __name__ == "__main__":
    sys.exit(main(sys.argv))
