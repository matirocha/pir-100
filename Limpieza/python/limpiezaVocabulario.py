#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os  
import re

#Script que dada la salida de Terrier sobre el Vocabulario devuelve archivo de estadisticas de Frecuencia x cantidad

def main(args):
	path = args[1]
	term_x_frec = {}
	frec_x_cant = {}
	archivo = open (path, "r")
	linea = archivo.readline()
	while linea != '':
		split = linea.split(',')
		linea = archivo.readline()
		frec = split[1].split(' ')[2].replace("TF=", "")
		term_x_frec[split[0].strip()] = frec
		if int(frec) >= 10:
			frec = 10
		try:
			frec_x_cant[frec] += 1
		except:
			frec_x_cant[frec] = 1


	stds = open("estadisticasVocabulario.txt", "w")
	resultado = sorted(frec_x_cant, key=int)
	for r in resultado:
		stds.write('	' + str(r) + '						' + str(frec_x_cant[r]) + '\n')

	stds.write('\n')
	archivo.close()
	stds.close()
		
if __name__ == "__main__":
   sys.exit(main(sys.argv))
	
	
