#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Script que dado un query log obtiene otro con solo terminos en el vocabulario
#Tiene como parametros de entrada los path de:
# (1) El querylog
# (2) El vocabulario (devuelto por Terrier)

import sys
import os  
import re

def main(args):
	pathQ = args[1] #Querys
	pathV = args[2]	#Vocabulario
	archivo = open (pathV, "r")
	vocabulario = {}
	linea = archivo.readline()
	while linea != '':
		split = linea.split(',')
		vocabulario[split[0].strip()] = 0
		linea = archivo.readline()
	

	archivo.close()
	logN = open ('queryLog.txt', "w+")	#Archivo de querys con tiempo sesgados
	qlog = open (pathQ, "r")
	unicas = {}
	band = True
	linea = qlog.readline()
	while linea != '':
		palabras = linea.split(" ")
		for pal in palabras:
			try:
				vocabulario[pal.strip()]
			except:
				band = False		

		if band:
			logN.write('%s\n' %(linea.strip()))
			unicas[linea.strip()] = 0

		band = True
		linea = qlog.readline()

	c = 0
	topics = open('queryTREC.txt', "w+")	#Archivo de querys con tiempo sesgados
	for q in unicas:
		c += 1
		l = """<TOP>
<NUM>%d</NUM>
<TITLE>%s</TITLE>
</TOP>
""" %(c, str(q))
		topics.write(l)


	print "Proceso terminado"
		
		
		
if __name__ == "__main__":
   sys.exit(main(sys.argv))
