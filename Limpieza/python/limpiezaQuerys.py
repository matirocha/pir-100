#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Script que limpia el archivo de topics tomado por Terrier
#Tiene como parametro de entrada el path del topics.trec


import sys
import os  
import re

def main(args):
	path = args[1]
	archivo = open (path, "r")
	querysT = open ('querysTotales.txt', "w+")	#Archivo de querys con tiempo sesgados
	linea = archivo.readline()	#leo linea1
	while linea != '':	#mientras no sea fin
		if (re.search("TITLE",  linea)):
			cleanr = re.compile('<.*?>')
			query = re.sub(cleanr, '', linea)
			querysT.write("%s\n" %(query.strip()))
		linea = archivo.readline()	#leo linea1
		
		

	querysT.close()
	archivo.close()
	print "Proceso terminado"
		
		
		
if __name__ == "__main__":
   sys.exit(main(sys.argv))
