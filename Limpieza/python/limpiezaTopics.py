#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Descripción: 	Script de limpieza y obtención de costos de una determinada query. 
				Dado los archivos de topics y de respuesta de Terrier, se obtiene un archivo de costos.

Parametros:		python limpiezaTopics.py /path/rtaTerrier.txt /path/topics.txt
Autor: Matías Rocha
"""

import sys
import os  
import re


def main(args):
	t = 0	#Contador de query total
	c = 0
	timeT = {} #Diccionario tiempos totales
	timeS = {} #Diccionario tiempos sesgados
	listD = []
	lenList = (len(args))/2
	for i in range (1,lenList+1):
		n = 2*i
		listD.append(n)
		
	for i in listD:
		topics = {}
		path = args[i-1]
		archivo = open (path, "r") #rta
		path2 = args[i]
		archivo2 = open (path2, "r")	#topics
		

		#Creo un diccionario con el numero de query y su query
		linea = archivo2.readline()	#leo linea 1
		while linea != '':	#mientras no sea fin
			linea = archivo2.readline()#linea 2
			cleanr = re.compile('<.*?>')
			n2 = re.sub(cleanr, '', linea)
			linea = archivo2.readline() #linea 3
			q = re.sub(cleanr, '', linea)
			topics[n2.strip()] = q.strip()
			linea = archivo2.readline()	#leo linea 4
			linea = archivo2.readline()	#leo linea 1
		


		time_x_term = {}
		cant_terms = {}
		#Recorro archivo arrojado por terrier
		bandS = False
		#m = "time to intialise index"	#Luego de esta linea comienzan las querys
		#linea = archivo.readline()	#leo linea 1
		#Cuando encuentra coincidencia deja de leer y arranca a operar
		#while not(re.search(m,  linea)):
		#	linea = archivo.readline()	#leo linea 1
		linea = archivo.readline()	#leo linea siguiente dnde me voy a quedar con el numero de topics
		while linea != '':	#mientras no sea fin
			t += 1
			lineaquery = linea
			while not(re.search("posting lists",  linea)):
				linea = archivo.readline()	#leo linea 2
			split = linea.split(" ")	#spliteo por espacio
			

			if split[9] == split[12]:
				#Pregunto si el numero PL es igual a la cantidad de querys
				bandS = True
			while not(re.search("Time to process query",  linea)):
				linea = archivo.readline()	#leo linea 2
			time = linea.split()[9] #ME quedo con el tiempo de procesamietno
			print split[9]
			if not(time == "0.0"):
				#c += 1
				#Si el tiempo no es cero

				#Creo estadisiticas
				try:
					time_x_term[split[9]] += float(time)
					c += 1
				except:
					time_x_term[split[9]] = float(time)

				try:
					cant_terms[split[9]] += 1
				except:
					cant_terms[split[9]] = 1

				split = lineaquery.split(" ")	#spliteo por tab la query que lei al principio
				n = split[6].strip()
				if bandS:
					#Si la cantidad de terminos son iguales a la cantidad de PL
					timeS[topics[n]] = time.strip()
					#tiempos.write("%s\n" %(query.strip()))
					bandS = False
				timeT[topics[n]] = time.strip()
				#tiemposT.write("%s\n" %(query.strip()))
			linea = archivo.readline()	#leo linea
		archivo.close()
		archivo2.close()
	tiemposS = open ('tiemposSegados.txt', "w+")	#Archivo de querys con tiempo sesgados
	tiemposT = open ('tiemposTodos.txt', "w+")	#Archivo de querys con tiempo de todas
	for i in timeS:
		tiemposS.write("%s\t:\t%s\n" %(i, timeS[i]))
	for i in timeT:
		tiemposT.write("%s\t:\t%s\n" %(i, timeT[i]))
	
	
	tiemposS.close()
	tiemposT.close()
	log = open('estadisticasTopics.log', 'w+') 
	log.write("Total queries: %s\n" %(str(t))) 
	log.write("Total queries con costo cero: %s\n" %(str(c))) 
	log.write("Total procesadas: %s\n" %(str(len(timeT)))) 
	log.write("Total sesgadas: %s\n" %(str(len(timeS))))
	log.write('------------------------------------------------------------\n')
	#Lo ordeno por cantidad de palabras
	resultado = sorted(time_x_term, key=int)
	#Imprimo la cantidad de terminos que tiene una query, y su tiempo promedio de resolucion
	log.write('Costo promedio de resolucion de query por cantidad de terminos\n')
	for r in resultado:
		log.write((str(r) + '\t\t\t\t\t\t %.2f \n') %((time_x_term[r]/cant_terms[r])))
	log.write('------------------------------------------------------------\n')


	log.close()
	print "Proceso terminado"
	#mail = mailClass.mailClass("matu.rocha@gmail.com", "matu.rocha@gmail.com", "Proceso terminado")
	#mail.enviarMail()
		
		
		
if __name__ == "__main__":
   sys.exit(main(sys.argv))
	
	
