#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os  
import re


def main(args):
	path = args[1]
	archivo = open(path,"r")
	c = 0
	linea = archivo.readline()
	while linea != '':
		if linea.startswith( '<DOCNO>' ):
			c += 1

		linea = archivo.readline()
	print "Total Docs: ", c

if __name__ == "__main__":
   sys.exit(main(sys.argv))
	
	
