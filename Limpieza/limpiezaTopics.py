#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os  
import re


def main(args):
	t = 0	#Contador de query total
	timeT = {} #Diccionario tiempos totales
	timeS = {} #Diccionario tiempos sesgados
	listD = []
	lenList = (len(args))/2
	for i in range (1,lenList+1):
		n = 2*i
		listD.append(n)
		
	for i in listD:
		topics = {}
		path = args[i-1]
		archivo = open (path, "r") #rta
		path2 = args[i]
		archivo2 = open (path2, "r")	#topics
		
		linea = archivo2.readline()	#leo linea 1
		while linea != '':	#mientras no sea fin
			linea = archivo2.readline()#linea 2
			cleanr = re.compile('<.*?>')
			n2 = re.sub(cleanr, '', linea)
			linea = archivo2.readline() #linea 3
			q = re.sub(cleanr, '', linea)
			topics[n2.strip()] = q.strip()
			linea = archivo2.readline()	#leo linea 4
			linea = archivo2.readline()	#leo linea 1
		
		bandS = False
		m = "time to intialise index"	#Luego de esta linea comienzan las querys
		linea = archivo.readline()	#leo linea 1
		#Cuando encuentra coincidencia deja de leer y arranca a operar
		while not(re.search(m,  linea)):
			linea = archivo.readline()	#leo linea 1
		linea = archivo.readline()	#leo linea siguiente dnde me voy a quedar con el numero de topics
		while linea != '':	#mientras no sea fin
			t += 1
			lineaquery = linea
			while not(re.search("posting lists",  linea)):
				linea = archivo.readline()	#leo linea 2
			split = linea.split(" ")	#spliteo por espacio
			if split[9] == split[12]:
				#Pregunto si el numero PL es igual a la cantidad de querys
				bandS = True
			while not(re.search("Time to process query",  linea)):
				linea = archivo.readline()	#leo linea 2
			time = linea.split()[9] #ME quedo con el tiempo de procesamietno
			if not(time == "0.0"):
				split = lineaquery.split(" ")	#spliteo por tab la query que lei al principio
				n = split[6]
				if bandS:
					timeS[topics[n]] = time.strip()
					#tiempos.write("%s\n" %(query.strip()))
					bandS = False
				timeT[topics[n]] = time.strip()
				#tiemposT.write("%s\n" %(query.strip()))
			linea = archivo.readline()	#leo linea
		archivo.close()
		archivo2.close()
	tiemposS = open ('tiemposSegados.txt', "w+")	#Archivo de querys con tiempo sesgados
	tiemposT = open ('tiemposTodos.txt', "w+")	#Archivo de querys con tiempo de todas
	for i in timeS:
		tiemposS.write("%s\t:\t%s\n" %(i, timeS[i]))
	for i in timeT:
		tiemposT.write("%s\t:\t%s\n" %(i, timeT[i]))
	
	
	tiemposS.close()
	tiemposT.close()
	log = open('estadisticasTopics.log', 'w+') 
	log.write("Total queries: %s\n" %(str(t))) 
	log.write("Total procesadas: %s\n" %(str(len(timeT)))) 
	log.write("Total sesgadas: %s\n" %(str(len(timeS))))
	log.close()
	print "Proceso terminado"
	#mail = mailClass.mailClass("matu.rocha@gmail.com", "matu.rocha@gmail.com", "Proceso terminado")
	#mail.enviarMail()
		
		
		
if __name__ == "__main__":
   sys.exit(main(sys.argv))
	
	
