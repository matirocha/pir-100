#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

def main(args):
	path = args[1]
	pathCosto = args[2]
	archivo = open (path, "r")
	linea = archivo.readline()	#Primera linea descarto.
	query = {}	#lista que contendra las querys propiamente dicha
	c = 0

	while linea != '':	#mientras no sea fin
		try:
			query[linea.strip()] += 1
		except:
			query[linea.strip()] = 1

		linea = archivo.readline()	#leo otra linea

	archivo.close()

	costos = {}
	try:
		archivo = open (pathCosto, "r")
	except e:
		print "Error de lectura: " + e
	linea = archivo.readline()	#Leo linea del archivo de info
	print "Cargando costos en memoria"
	while linea != "":
		split = linea.split("\t:\t")	#Spliteo por tab
		costos[split[0].strip()] = float(split[1].strip())	#Cargo en la query con su respectivo costo
		linea = archivo.readline()	#Leo linea del archivo de info
	print "Proceso terminado"
	archivo.close()

	faltan = []
	c = 0
	for q in query.keys():
		print q
		if not(q in costos.keys()):
			c += 1
			faltan.append(q.strip())

	print "Faltan: ", c

	faltan = open ("falta.txt", "r")
	for q in faltan:
		faltan.write(q)

	faltan.close()




if __name__ == "__main__":
   sys.exit(main(sys.argv))